
(* define expansion for scalar integrals *)
$Assumptions = m12 > 0 && \[Delta] > 0 && p2 > 0 && \[Mu]UV2 > 0 && \[Mu]IR2 > 0;
maxorder = 10;
C0c2[a_, n_, p2_] := (Sum[(-1)^k (1 - a)^(k - n)/a^k/(n - k), {k, 0, n - 1}]/a + (-1)^n/a^(n + 1) (-Log[1 - a])) a^(n + 1)/(n + 1)/p2
(*B0Sol[p2_, \[Mu]2_, z_] := -Log[p2/\[Mu]2] - Integrate[Log[-x^2 + x (1 + z)], {x, 0, 1}];*)
B0zero = Log[\[Mu]UV2/\[Mu]IR2];
A0Sol[\[Mu]2_, m2_] := m2 (-Log[m2/\[Mu]2] + 1);
(*B0Exp = Collect[Normal[Series[B0Sol[p2 (1 + \[Delta]), \[Mu]UV2, m12/p2/(1 + \[Delta])], {\[Delta], 0, maxorder}]],\[Delta],FullSimplify];*)
B0C[p2_, z_, \[Mu]UV2_, n_] := If[n == 0, 2 + Log[\[Mu]UV2/p2/(z + 1)] + z Log[z/(z + 1)],
                                  (-1)^n (z Log[z/(z + 1)] + (1/(z + 1))^n/n +  z  Sum[(1/(z + 1))^k/k, {k, 1, n}])]



B0V[p2_,m12_,d_, \[Mu]UV2_, start_,end_]:=Sum[d^(k-start) B0C[p2, m12/p2, \[Mu]UV2, k], {k,start,end}]
C0V[p2_,m12_,d_,start_,end_]:=Sum[d^(k-start) C0c2[-p2/m12, k, p2], {k,start,end}]


(* construct substitution rules *)
SIrepl = {A0[m1] -> A0Sol[\[Mu]UV2, m12],
          B0[0, 0, 0] -> B0zero, 
          B0[-p2, 0, m1] -> B0C[p2, m12/p2, \[Mu]UV2, 0], 
          Derivative[n_, 0, 0][B0][-p2, 0, m1] :> B0C[p2, m12/p2, \[Mu]UV2, n]/(-p2)^n n!,
          (*Derivative[n_, 0, 0][B0][-p2, 0, m1] :> Coefficient[If[n == 0, \[Delta], 1] (B0Exp), If[n == 0, \[Delta], \[Delta]^n]]/(-p2)^n n!, *)
          C0[-p2, -p2, 0, m1, m1] -> C0c2[-p2/m12, 0, p2], 
          Derivative[0, n_, 0, 0, 0][C0][-p2, -p2, 0, m1, m1] :> C0c2[-p2/m12, n, p2]/(-p2)^n n!};
