(* define expansion for scalar integrals *)
$Assumptions = m02 > 0 && \[Delta] > 0 && p2 > 0 && \[Mu]UV2 > 0 && \[Mu]IR2 > 0;

C0c1PoleIR[a_, n_, \[Epsilon]_] := -1/((1 + n) \[Epsilon]) (-a)^(1 + n)
C0c1[a_, n_, p2_, \[Mu]2_] := -1/((1 + n)) (-a)^(1 + n) (HarmonicNumber[n] + HarmonicNumber[1 + n] + Log[1 - a] - Log[1/a])/ p2 + (-1)^(n + 1)/((1 + n)) (Log[1 - a] + Sum[a^(k + 1)/(k + 1), {k, 0, n}])/p2 - C0c1PoleIR[a, n, 1] Log[p2/\[Mu]2]/p2
B0zero = Log[\[Mu]UV2/\[Mu]IR2];
A0Sol[\[Mu]2_, m2_] := m2 (-Log[m2/\[Mu]2] + 1);
B0C[p2_, z_, \[Mu]UV2_, n_] := If[n == 0, 2 + Log[\[Mu]UV2/p2/(z + 1)] + z Log[z/(z + 1)],
                                  (-1)^n (z Log[z/(z + 1)] + (1/(z + 1))^n/n +  z  Sum[(1/(z + 1))^k/k, {k, 1, n}])]


(* construct substitution rules *)
SIrepl = {A0[m0] -> A0Sol[\[Mu]UV2, m02],
          B0[0, 0, 0] -> B0zero, 
          B0[-p2, m0, 0] -> B0C[p2, m02/p2, \[Mu]UV2, 0], 
          Derivative[n_, 0, 0][B0][-p2, m0, 0] :> B0C[p2, m02/p2, \[Mu]UV2, n]/(-p2)^n n!,
          C0[-p2, -p2, m0, 0, 0] -> C0c1[1/(1 + m02/p2), 0, p2, \[Mu]IR2], 
          Derivative[0, n_, 0, 0, 0][C0][-p2, -p2, m0, 0, 0] :> C0c1[1/(1 + m02/p2), n, p2, \[Mu]IR2]/(-p2)^n n!
         };
