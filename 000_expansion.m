(* define expansion for scalar integrals *)
$Assumptions = \[Delta] > 0 && p2 > 0 && \[Mu]UV2 > 0 && \[Mu]IR2 > 0;
maxorder = 100;

C0c0PoleIR[n_, \[Epsilon]_] := 1/((1 + n) \[Epsilon] p2 ) (-1)^(1 + n)
C0c0finite[k_, p2_, \[Mu]2_] := 1/(2 p2) (-Sum[ (-1)^(n)/(n + 1) *(-1)^(k - n + 1)/(k - n), {n, 0, k - 1}] - 2 ((-1)^(k)/(k + 1) ) Log[ p2 / \[Mu]IR2 ])
B0Sol[p2_, \[Mu]2_, z_] := -Log[p2/\[Mu]2] - Integrate[Log[-x^2 + x (1 + z)], {x, 0, 1}];
B0zero = Log[\[Mu]UV2/\[Mu]IR2];
B0Exp = Normal[Series[B0Sol[p2 (1 + \[Delta]), \[Mu]UV2, 0 ], {\[Delta], 0, maxorder}]];


(* construct substitution rules *)
SIrepl = {B0[0, 0, 0] -> B0zero, 
          B0[-p2, 0, 0] -> B0Sol[p2, \[Mu]UV2, 0], 
          Derivative[n_, 0, 0][B0][-p2, 0, 0] :> Coefficient[If[n == 0, \[Delta], 1] (B0Exp), If[n == 0, \[Delta], \[Delta]^n]]/(-p2)^n n!, 
          C0[-p2, -p2, 0, 0, 0] -> C0c0finite[0, p2, \[Mu]IR2], 
          Derivative[0, n_, 0, 0, 0][C0][-p2, -p2, 0, 0, 0] :> C0c0finite[n, p2, \[Mu]IR2] n!/(-p2)^n
         };
