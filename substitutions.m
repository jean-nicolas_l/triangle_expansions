SetAttributes[{g, P11, P22, P12}, Orderless];

DefRules = {
   P112[\[Mu]_, \[Nu]_, \[Rho]_] -> (p1[\[Mu]] p1[\[Nu]] p2[\[Rho]] + p1[\[Rho]] p1[\[Mu]] p2[\[Nu]] + p1[\[Rho]] p1[\[Nu]] p2[\[Mu]])/p2,
   P122[\[Mu]_, \[Nu]_, \[Rho]_] -> (p2[\[Mu]] p2[\[Nu]] p1[\[Rho]] + p2[\[Rho]] p2[\[Mu]] p1[\[Nu]] + p2[\[Rho]] p2[\[Nu]] p1[\[Mu]])/p2,
   P111[\[Mu]_, \[Nu]_, \[Rho]_] -> (p1[\[Mu]] p1[\[Nu]] p1[\[Rho]])/p2,
   P222[\[Mu]_, \[Nu]_, \[Rho]_] -> (p2[\[Mu]] p2[\[Nu]] p2[\[Rho]])/p2,
   gp1[\[Mu]_, \[Nu]_, \[Rho]_] -> g[\[Mu], \[Nu]] p1[\[Rho]] + g[\[Mu], \[Rho]] p1[\[Nu]] + g[\[Nu], \[Rho]] p1[\[Mu]],
   gp2[\[Mu]_, \[Nu]_, \[Rho]_] -> g[\[Mu], \[Nu]] p2[\[Rho]] + g[\[Mu], \[Rho]] p2[\[Nu]] + g[\[Nu], \[Rho]] p2[\[Mu]]
   }


TensorsRules = {
   (* rank 1*)
   P12[\[Mu]_] -> p1[\[Mu]] - p2[\[Mu]],
   
   (* rank 2 *)
   P11[\[Mu], \[Nu]] -> (p1[\[Mu]] p1[\[Nu]])/p2,
   P22[\[Mu], \[Nu]] -> (p2[\[Mu]] p2[\[Nu]])/p2,
   P12[\[Mu], \[Nu]] -> (p1[\[Nu]] p2[\[Mu]] + p1[\[Mu]] p2[\[Nu]])/p2,
   T[\[Mu]_, \[Nu]_] -> ((p1[\[Mu]] - p2[\[Mu]]) (p1[\[Nu]] - p2[\[Nu]]))/p2,
   U[\[Mu]_, \[Nu]_] -> (p1[\[Mu]] p1[\[Nu]] - p2[\[Mu]] p2[\[Nu]])/p2,
   
   (* rank 3 *)   
   P112[\[Mu]_, \[Nu]_, \[Rho]_] -> (p1[\[Mu]] p1[\[Nu]] p2[\[Rho]] + p1[\[Rho]] p1[\[Mu]] p2[\[Nu]] + p1[\[Rho]] p1[\[Nu]] p2[\[Mu]])/p2,
   P122[\[Mu]_, \[Nu]_, \[Rho]_] -> (p2[\[Mu]] p2[\[Nu]] p1[\[Rho]] + p2[\[Rho]] p2[\[Mu]] p1[\[Nu]] + p2[\[Rho]] p2[\[Nu]] p1[\[Mu]])/p2,
   P111[\[Mu]_, \[Nu]_, \[Rho]_] -> (p1[\[Mu]] p1[\[Nu]] p1[\[Rho]])/ p2,
   P222[\[Mu]_, \[Nu]_, \[Rho]_] -> (p2[\[Mu]] p2[\[Nu]] p2[\[Rho]])/ p2,
   W0[\[Mu]_, \[Nu]_, \[Rho]_] -> -2 P111[\[Mu], \[Nu], \[Rho]] + P112[\[Mu], \[Nu], \[Rho]] - P222[\[Mu], \[Nu], \[Rho]],
   W1[\[Mu]_, \[Nu]_, \[Rho]_] -> P111[\[Mu], \[Nu], \[Rho]] - P222[\[Mu], \[Nu], \[Rho]],
   W2[\[Mu]_, \[Nu]_, \[Rho]_] -> -P112[\[Mu], \[Nu], \[Rho]] + P122[\[Mu], \[Nu], \[Rho]] + P111[\[Mu], \[Nu], \[Rho]] - P222[\[Mu], \[Nu], \[Rho]],
   gp1[\[Mu]_, \[Nu]_, \[Rho]_] -> g[\[Mu], \[Nu]] p1[\[Rho]] + g[\[Mu], \[Rho]] p1[\[Nu]] + g[\[Nu], \[Rho]] p1[\[Mu]],
   gp2[\[Mu]_, \[Nu]_, \[Rho]_] -> g[\[Mu], \[Nu]] p2[\[Rho]] + g[\[Mu], \[Rho]] p2[\[Nu]] + g[\[Nu], \[Rho]] p2[\[Mu]],
   Gm[\[Mu]_, \[Nu]_, \[Rho]_] -> (gp1[\[Mu], \[Nu], \[Rho]] - gp2[\[Mu], \[Nu], \[Rho]])/12,
   Gp[\[Mu]_, \[Nu]_, \[Rho]_] -> (gp1[\[Mu], \[Nu], \[Rho]] + gp2[\[Mu], \[Nu], \[Rho]])/12
   (* alternative definitions of W2: *)
   (*W2[\[Mu]_,\[Nu]_,\[Rho]_] -> ((p1[\[Mu]]-p2[\[Mu]])(p1[\[Nu]]- p2[\[Nu]])(p1[\[Rho]]-p2[\[Rho]]))/p2 *)
   };

factrules = {
   z0 -> m02 /p2,
   z1 -> m12/p2,
   z -> m2/p2
   };
