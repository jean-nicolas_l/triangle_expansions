(* define expansion for scalar integrals *)
$Assumptions = z > 1/4 && m2 > 0 && \[Delta] > 0 && p2 > 0 && \[Mu]UV2 > 0 && \[Mu]IR2 > 0;
maxorder = 15;
C0c3[p2_, z_, n_] := -(-1)^(n)/(n + 1)! D[(z^n Log[(1 + Sqrt[1 + 4 z])/(-1 + Sqrt[1 + 4 z])]/Sqrt[1 + 4 z]), {z, n}]/p2
A0Sol[\[Mu]2_, m2_] := m2 (-Log[m2/\[Mu]2] + 1);
B0Sol[p2_, \[Mu]2_, z_] := -Log[p2/\[Mu]2] - (Log[z] + 2 Sqrt[4 z + 1] ((1/2)*Log[1/Sqrt[4*z + 1] + 1] - (1/2)*Log[1 - 1/Sqrt[4*z + 1]]) - 2);
B0Exp = Normal[Series[B0Sol[p2 (1 + \[Delta]), \[Mu]UV2, m2/p2/(1 + \[Delta])], {\[Delta], 0, maxorder}]];

(* construct substitution rules *)
SIrepl = {A0[m] -> A0Sol[\[Mu]UV2, m2],
          B0[-p2, m, m] -> B0Sol[p2, \[Mu]UV2, m2/p2], 
          Derivative[n_, 0, 0][B0][-p2, m, m] :> Coefficient[If[n == 0, \[Delta], 1] (B0Exp), If[n == 0, \[Delta], \[Delta]^n]]/(-p2)^n n!, 
          C0[-p2, -p2, m, m, m] -> C0c3[ p2, z, 0], 
          Derivative[0, n_, 0, 0, 0][C0][-p2, -p2, m, m, m] :> C0c3[p2, z, n]/(-p2)^n n!
         };
