program demo

  use COLLIER

  implicit none

  double complex :: m02,m12,m22,m32,m42,m52
  double complex :: p10,p21,p32,p43,p54,p50,p20,p31
  double complex :: p42,p53,p40,p51,p30,p41,p52

  
  integer, parameter :: rmax = 3
  double complex :: C(0:rmax/2,0:rmax,0:rmax)
  double complex :: Cuv(0:rmax/2,0:rmax,0:rmax)

  double complex :: C0
  double precision :: delta


  call Init_cll(4,noreset=.true.)


  delta = .01d0

  p10 = complex(-3.d0,0d0)
  p21 = complex(0d0,0d0)
  p20 = complex(-3d0*(1d0+delta),0d0)

  ! z0 != 0, z = 0: collinear divergent case
  !m02 = complex(1d0,0d0)
  m02 = complex(170d0**2,0d0)
  m12 = complex(0d0,0d0)
  m22 = complex(0d0,0d0)

  ! z0 = 0, z != 0: simple case & no collinear divergence 
  !m02 = complex(0d0,0d0)
  !m12 = complex(1d0,0d0)
  !m22 = complex(1d0,0d0)

  !! equal mass case
  !m02 = complex(3d0,0d0)
  !m12 = complex(3d0,0d0)
  !m22 = complex(3d0,0d0)


  call C0_cll(C0,p10,p21,p20,m02,m12,m22)

  write(*,*) "C0:", C0

  call B0_cll(C0,p10,m02,m12)
  write(*,*) "B0:", C0




  ! C tensor
  call C_cll(C,Cuv,p10,p21,p20,m02,m12,m22,rmax)

  write(*,*) "C0:", C(0,0,0)

  write(*,*) "C1:", C(0,1,0)
  write(*,*) "C2:", C(0,0,1)

  write(*,*) "C00:", C(1,0,0)
  write(*,*) "C11:", C(0,2,0)
  write(*,*) "C22:", C(0,0,2)
  write(*,*) "C12:", C(0,1,1)

  write(*,*) "C110:", C(1,1,0)
  write(*,*) "C030:", C(0,3,0)
  write(*,*) "C030:", C(0,0,3)
 
end program demo
