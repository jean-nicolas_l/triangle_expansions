from __future__ import print_function
from pySecDec.integral_interface import IntegralLibrary
from sympy import sympify

pse = IntegralLibrary('triangle1L/triangle1L_pylink.so')
pse.use_Vegas(flags=0,epsrel=0.00001, epsabs=1e-08, maxeval=10000000)

delta = 0.1
m0 = 0.
m1 = 5.
m2 = 5.
p12 = -100.
p22 = 0.
p32 = -100*(1+delta)


# m0 = 0.
# m1 = 5.
# m2 = 5.
# p12 = -100.
# p22 = 0.
# p32 = 30.

mu = 1.

s = pse(real_parameters=[p12, p22, p32, m0, m1, m2, mu])
expr = sympify(s[2].replace('+/-', '*value+error*'))
fin = expr.coeff('eps',0).coeff('value')
pole = expr.coeff('eps',-1).coeff('value')
print("fin:", fin)

# collier result
# (-1.57052187414205013E-002,-0.0000000000000000)
