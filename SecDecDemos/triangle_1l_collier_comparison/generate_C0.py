#! /usr/bin/env python
from pySecDec.loop_integral import loop_package
import pySecDec as psd

propagators = ['q**2-m0**2', '(q+p1)**2-m1**2', '(q-p3)**2-m2**2']
loop_momenta = ['q']
replacement_rules = [('p1*p1', 'p12'), ('p3*p3', 'p32'), ('p1*p3', '(p22-p12-p32)/2')]

li = psd.loop_integral.LoopIntegralFromPropagators(propagators, loop_momenta,
                                                   replacement_rules=replacement_rules)

# li = psd.loop_integral.LoopIntegralFromGraph(
# internal_lines = [['m',[1,2]],[0,[2,3]],[0,[3,4]],[0,[4,1]]],
# external_lines = [['p1',1],['p2',2],['p3',3],['p4',4]],
#
#
#

param = ['p12', 'p22', 'p32', 'm0', 'm1', 'm2', 'mu']

prefactor = 'mu**(2*eps) + eps*0.57721566490153286060651209008240243104215933593992'
# mass_symbols = ['msq']

loop_package(

name = 'triangle1L',

loop_integral = li,

real_parameters = param,

additional_prefactor=prefactor,

# the highest order of the final epsilon expansion --> change this value to whatever you think is appropriate
requested_order = 0,

# the optimization level to use in FORM (can be 0, 1, 2, 3)
form_optimization_level = 2,

# the WorkSpace parameter for FORM
form_work_space = '100M',

# the method to be used for the sector decomposition
# valid values are ``iterative`` or ``geometric`` or ``geometric_ku``
decomposition_method = 'geometric',
# if you choose ``geometric[_ku]`` and 'normaliz' is not in your
# $PATH, you can set the path to the 'normaliz' command-line
# executable here
#normaliz_executable='/path/to/normaliz',

)
